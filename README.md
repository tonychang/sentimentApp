# ![sentimentApp](https://tonychang.gitlab.io/sentimentApp/images/demo/demo_landing.JPG)
# sentimentApp
<table>
<tr>
<td>
  A webapp using Dash API to serve as a demonstration dashboard for the RNN prediction of data. We gather the entire public comments dataset from the ![www.regulations.gov](https://www.regulations.gov/document?D=DOI-2017-0002-0002) page for the National Monument review. 
</td>
</tr>
</table>


## Demo
Here is a working live demo :  https://tonychang.gitlab.io/sentimentApp/

## Site

### Landing Page
Currently is static and displays the output from the RNN language model.

![](https://tonychang.gitlab.io/sentimentApp/images/demo/web_app_face.JPG)

### Query Filled Form
![](https://tonychang.gitlab.io/sentimentApp/images/demo/demo_query.JPG)

### Charts
![](https://tonychang.gitlab.io/sentimentApp/images/demo/demo_chart1.JPG)
![](https://tonychang.gitlab.io/sentimentApp/images/demo/demo_chart2.JPG)
![](https://tonychang.gitlab.io/sentimentApp/images/demo/demo_chart3.JPG)


## Mobile support
The sentimentApp is compatible with devices of all sizes and all OS's, and consistent improvements are being made.

![](https://tonychang.gitlab.io/sentimentApp/images/demo/mobile.png)

## [Usage](https://tonychang.gitlab.io/sentimentApp/) 

### Development
Want to contribute? Great!

To fix a bug or enhance an existing module, follow these steps:

- Fork the repo
- Create a new branch (`git checkout -b improve-feature`)
- Make the appropriate changes in the files
- Add changes to reflect the changes made
- Commit your changes (`git commit -am 'Improve feature'`)
- Push to the branch (`git push origin improve-feature`)
- Create a Pull Request 

### Bug / Feature Request

If you find a bug (the website couldn't handle the query and / or gave undesired results), kindly open an issue [here](https://gitlab.com/tonychang/sentimentApp/issues/new) by including your search query and the expected result.

If you'd like to request a new function, feel free to do so by opening an issue [here](https://gitlab.com/tonychang/sentimentApp/issues/new). Please include sample queries and their corresponding results.


## Built with 

- [Dash](https://plot.ly/products/dash) - Dash is a Python framework for building analytical web applications. No JavaScript required.


## To-do
- Update webapp

## Team
[![Tony Chang](https://avatars1.gitlabusercontent.com/u/12688534?v=3&s=144)](https://gitlab.com/tonychang)  | [![David H. Smith Fellows](https://conbio.org/images/smith/HeaderLogo_noline1.jpg)](https://conbio.org/mini-sites/smith-fellows/)
---|---
[Tony Chang](https://gitlab.com/tonychang) |[Smith Fellows](https://conbio.org/mini-sites/smith-fellows/)

## [License](https://gitlab.com/tonychang/sentimentApp/blob/master/LICENSE.md)

CSP © [Tony Chang ](https://gitlab.com/tonychang)

