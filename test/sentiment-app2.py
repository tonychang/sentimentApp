import dash
from dash.dependencies import Input, Output
import dash_html_components as html
import dash_core_components as dcc
import pandas as pd
import numpy as np
import os
import plotly.graph_objs as go

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
df = pd.read_excel('/data/predictions_no_dup_v1.xlsx').iloc[:30000]
#remap negative sentiment to -1
df.replace(to_replace={'pred_sentiment':2}, value=-1, inplace=True)
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

#if 'DYNO' in os.environ:
#    app.scripts.append_script({
#        'external_url': 'https://cdn.rawgit.com/chriddyp/ca0d8f02a1659981a0ea7f013a378bbd/raw/e79f3f789517deec58f41251f7dbb6bee72c44ab/plotly_ga.js'
#    })

############

def add_markers( figure_data, idtags, plot_type = 'scatter3d'):
    indices = []
    fig_data = figure_data[0]
    for i in idtags:
        hover_text = fig_data['text']
        for j in range(len(hover_text)):
            if i == hover_text[j]:
                indices.append(i)

    if plot_type == 'histogram2d':
        plot_type = 'scatter'

    traces = []
    for point_number in indices:
        trace = dict(
            x = [fig_data['x'][point_number]],
            y = [fig_data['y'][point_number]],
            marker = dict(
                color = 'red',
                size = 3, 
                opacity = 0.6, 
                symbol = 'cross'
            ),
            type = plot_type
        )
        if plot_type == 'scatter3d':
            trace['z'] = [fig_data['z'][point_number]]
        
        traces.append(trace)

    return(traces)

BACKGROUND = 'rgb(230,230,230)'
COLORSCALE = [ [0, "rgb(244,236,21)"], [0.3, "rgb(249,210,41)"], [0.4, "rgb(134,191,118)"],
                [0.5, "rgb(37,180,167)"], [0.65, "rgb(17,123,215)"], [1, "rgb(54,50,153)"] ]

def rand_jit(arr):
    stdev = .01*(max(arr)-min(arr))
    return(arr + np.random.randn(len(arr)) * stdev)

def scatter_plot_3d(
    x = rand_jit(df['0_prob'] * 100),
    y = rand_jit(df['2_prob'] * 100),
    z = rand_jit(df['1_prob'] * 100),
    size = 3,
    color = df['pred_sentiment'],
    xlabel = 'Neutral Prob (%)', 
    ylabel = 'Negative Prob (%)', 
    zlabel = 'Positive Prob (%)', 
    plot_type = 'scatter3d', 
    markers = []):

    def axis_template_3d( title, type='linear' ):
        return dict(
            showbackground = True,
            backgroundcolor = BACKGROUND,
            gridcolor = 'rgb(255, 255, 255)',
            title = title,
            type = type,
            zerolinecolor = 'rgb(255, 255, 255)'
        )

    def axis_template_2d(title):
        return dict(
            xgap = 10, ygap = 10,
            backgroundcolor = BACKGROUND,
            gridcolor = 'rgb(255, 255, 255)',
            title = title,
            zerolinecolor = 'rgb(255, 255, 255)',
            color = '#444'
        )

    def blackout_axis( axis ):
        axis['showgrid'] = False
        axis['zeroline'] = False
        axis['color']  = 'white'
        return axis

    data = [ dict(
        x = x,
        y = y, 
        z = z, 
        mode = 'markers',   
        marker = dict(
            colorscale = COLORSCALE, 
            colorbar = dict( 
                title = "Classified Sentiment",
                tickmode = "array",
                tickvals = [-1,0,1],
                ticktext = ['Negative', 'Neutral', 'Positive']
            ),
            line = dict( color = '#444' ), 
            reversescale = True, 
            sizeref = 45,
            sizemode = 'diameter', 
            opacity = 0.7,
            size = size,
            color = color, 
        ),
        text = df['id'],
        type = plot_type,
    )]

    layout = dict(
    font = dict( family = 'Raleway' ),
    hovermode = 'closest',
    margin = dict( r=20, t=0, l=0, b=0 ),
    showlegend = False,
    scene = dict(
        xaxis = axis_template_3d( xlabel ),
        yaxis = axis_template_3d( ylabel ),
        zaxis = axis_template_3d( zlabel ),
        camera = dict(
            up=dict(x=0, y=0, z=1),
            center=dict(x=0, y=0, z=0),
            eye=dict(x=0.08, y=2.2, z=0.08)
            )
        )
    )

    if plot_type in ['histogram2d', 'scatter']:
        layout['xaxis'] = axis_template_2d(xlabel)
        layout['yaxis'] = axis_template_2d(ylabel)
        layout['plot_bgcolor'] = BACKGROUND
        layout['paper_bgcolor'] = BACKGROUND
        del layout['scene']
        del data[0]['z']

    if plot_type == 'histogram2d':
        # Scatter plot overlay on 2d Histogram
        data[0]['type'] = 'scatter'
        data.append( dict(
                x = x,
                y = y,
                type = 'histogram2d',
                colorscale = 'Greys',
                showscale = False
            ) )
        layout['plot_bgcolor'] = 'black'
        layout['paper_bgcolor'] = 'black'
        layout['xaxis'] = blackout_axis(layout['xaxis'])
        layout['yaxis'] = blackout_axis(layout['yaxis'])
        layout['font']['color'] = 'white'

    if len(markers) > 0:
        data = data + add_markers( data, markers, plot_type = plot_type )

    return dict( data=data, layout=layout )

def make_dash_table( selection ):
    ''' Return a dash defintion of an HTML table for a Pandas dataframe '''
    df_subset = df.loc[df['id'].isin(selection)]
    table = []
    for index, row in df_subset.iterrows():
        html_row = []
        for i in range(len(row)):
            if i == 0 or i == 6:
                html_row.append( html.Td([ row[i] ]) )
            elif i == 1:
                html_row.append( html.Td([ html.A( href=row[i], children='Datasheet' )]))
            #elif i ==2:
            #    html_row.append( html.Td([ html.Img( src=row[i] )]))
        table.append( html.Tr( html_row ) )
    return table

FIGURE = scatter_plot_3d()
STARTING_ID = 'DOI-2017-0002-0002'
TEXT_DESCRIPTION = df.loc[df['id'] == STARTING_ID]['text'].iloc[0]

app.layout = html.Div([
    # Row 1: Header and Intro text

    html.Div([
        html.Img(src="https://conbio.org/images/smith/HeaderLogo_noline1.jpg",
                style={
                    'height': '100px',
                    'float': 'left',
                    'position': 'relative',
                    'bottom': '10px',
                    'left': '10px'
                },
                ),
        html.H2('National Monuments',
                style={
                    'position': 'relative',
                    'top': '0px',
                    'left': '10px',
                    'font-family': 'Dosis',
                    'display': 'inline',
                    'font-size': '6.0rem',
                    'color': '#4D637F'
                }),
        html.H2('',
                style={
                    'position': 'relative',
                    'top': '0px',
                    'left': '20px',
                    'font-family': 'Dosis',
                    'display': 'inline',
                    'font-size': '4.0rem',
                    'color': '#4D637F'
                }),
        html.H2('Sentiment Analysis',
                style={
                    'position': 'relative',
                    'top': '0px',
                    'left': '27px',
                    'font-family': 'Dosis',
                    'display': 'inline',
                    'font-size': '6.0rem',
                    'color': '#4D637F'
                }),
    ], className='row twelve columns', style={'position': 'relative', 'right': '15px'}),

    html.Div([
        html.Div([
            html.Div([
                html.P('HOVER over a point in the graph to the right to see its description'),
                #html.P('SELECT a point in the dropdown to add it to the text candidates at the bottom.')
            ], style={'margin-left': '10px'}),
            dcc.Dropdown(id='id_dropdown',
                        multi=True,
                        value=[ STARTING_ID ],
                        options=[{'label': i, 'value': i} for i in df['id'].tolist()]),
            ], className='twelve columns' )

    ], className='row' ),

    # Row 2: Hover Panel and Graph

    html.Div([
        html.Div([
            html.Div([
                dcc.Graph(id='prob_vals')], 
                style=dict(display= 'inline-block', width='49%')),

            html.Br(),
            #html <a> type
            html.A(STARTING_ID,
                  id='id',
                  href="https://www.regulations.gov/document?D=DOI-2017-0002-0001",
                  target="_blank"),

            #html <p> type
            html.P(TEXT_DESCRIPTION,
                  id='text',
                  style=dict( maxHeight='400px', fontSize='12px' )),

        ], className='four columns', style=dict(height='300px') ),

        html.Div([
            dcc.RadioItems(
                id = 'charts_radio',
                options=[
                    dict( label='3D Scatter', value='scatter3d' ),
                    dict( label='2D Scatter', value='scatter' ),
                    dict( label='2D Histogram', value='histogram2d' ),
                ],
            labelStyle = dict(display='inline'),
                value='scatter3d'
            ),

            dcc.Graph(id='clickable-graph',
                      style=dict(width='700px'),
                      hoverData=dict( points=[dict(pointNumber=0)] ),
                      figure=FIGURE ),

        ], className='eight columns', style=dict(textAlign='center')),


    ], className='row' ),

    html.Div([
        html.Table( make_dash_table( [STARTING_ID] ), id='table-element' )
    ])

], className='container')


@app.callback(
    Output('clickable-graph', 'figure'),
    [Input('id_dropdown', 'value'),
    Input('charts_radio', 'value')])

def highlight_point(id_dropdown_values, plot_type):
    return scatter_plot_3d( markers = id_dropdown_values, plot_type = plot_type )


@app.callback(
    Output('table-element', 'children'),
    [Input('id_dropdown', 'value')])
def update_table(id_dropdown_value):
    table = make_dash_table( id_dropdown_value )
    return table


def dfRowFromHover( hoverData ):
    ''' Returns row for hover point as a Pandas Series '''
    if hoverData is not None:
        if 'points' in hoverData:
            firstPoint = hoverData['points'][0]
            if 'pointNumber' in firstPoint:
                point_number = firstPoint['pointNumber']
                id_name = str(FIGURE['data'][0]['text'][point_number]).strip()
                return df.loc[df['id'] == id_name]
    return pd.Series()

@app.callback(
    Output('id', 'children'),
    [Input('clickable-graph', 'hoverData')])
def return_id_name(hoverData):
    if hoverData is not None:
        if 'points' in hoverData:
            firstPoint = hoverData['points'][0]
            if 'pointNumber' in firstPoint:
                point_number = firstPoint['pointNumber']
                id_name = str(FIGURE['data'][0]['text'][point_number]).strip()
                return id_name

def create_pie_plot(piedata):
    return html.Div([
        dcc.Graph(
        figure = dict(
            data = piedata,    
                )
            )
        ])

@app.callback(
    Output('prob_vals', 'figure'),
    [Input('clickable-graph', 'hoverData')])
def return_prob_vals(hoverData):
    if hoverData is not None:
        if 'points' in hoverData:
            firstPoint = hoverData['points'][0]
            if 'pointNumber' in firstPoint:
                point_number = firstPoint['pointNumber']
                neg_v = FIGURE['data'][0]['x'][point_number]
                pos_v = FIGURE['data'][0]['y'][point_number]
                neu_v = FIGURE['data'][0]['z'][point_number]
                dff = [pos_v, neu_v, neg_v]
                l = ['Neg', 'Neu', 'Pos']
                piedata = [dict(
                    y=dff,
                    x=l,
                    type='bar')]
                return create_pie_plot(piedata)

@app.callback(
    dash.dependencies.Output('id', 'href'),
    [dash.dependencies.Input('clickable-graph', 'hoverData')])
def return_href(hoverData):
    row = dfRowFromHover(hoverData)
    if row.empty:
        return
    datasheet_link = row['url'].iloc[0]
    return datasheet_link


@app.callback(
    Output('text', 'children'),
    [Input('clickable-graph', 'hoverData')])
def display_point(hoverData):
    row = dfRowFromHover(hoverData)
    if row.empty:
        return
    description = row['text'].iloc[0]
    return description

if __name__ == '__main__':
    app.scripts.config.serve_locally=True
    app.run_server(host='0.0.0.0',debug=True)
    app.config['suppress_callback_exceptions']=True
