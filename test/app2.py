import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import subprocess

subprocess.call('pip3 install xlrd', shell=True, stdout=subprocess.PIPE)
df = pd.read_excel('/data/predictions_no_dup_v1.xlsx')


def generate_table(dataframe, max_rows=1000):
    return html.Table(
        # Header
        [html.Tr([html.Th(col) for col in dataframe.columns])] +

        # Body
        [html.Tr([
            html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
        ]) for i in range(min(len(dataframe), max_rows))]
    )


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div(children=[
    html.H4(children='Sentiment Data'),
    generate_table(df)
])

if __name__ == '__main__':
    app.scripts.config.serve_locally=True
    app.run_server(host='0.0.0.0',debug=True)

