# -*- coding: utf-8 -*-

import dash
import dash_core_components as dcc
import dash_html_components as html
import numpy as np
import pandas as pd
external_stylesheets = ['https:/codepen.io/chriddyp/pen/bWLwgP.css']

#declare data
data_a = pd.DataFrame(np.random.randint(0,10,(3,2)),columns=['x','y'])
data_b = pd.DataFrame(np.random.randint(0,10,(3,2)),columns=['x','y'])

#declare the app
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

colors = {\
        'background': '#111111',\
        'text': '#7FCBFF'
}

#declare the layout
app.layout = html.Div(\
    children=[\
    html.H1(\
        children='Hello World',\
        style={\
            'textAlign':'center',\
            'color': colors['text']
        }
    ),\
    html.Div(\
        children='''CSP Dash: Testing this web application framework.''',\
        style={\
            'textAlign':'center',\
            'color': colors['text']
        }
    ),\
    dcc.Graph(\
        id='example-graph',\
        figure={\
            'data': [\
                {'x':data_a.x,\
                'y':data_a.y, \
                'type': 'bar', \
                'name': 'data a'},\
                {'x':data_b.x, \
                'y':data_b.y, \
                'type': 'bar', \
                'name': 'data b'}],
            'layout':{\
                'plot_bgcolor': colors['background'],\
                'paper_bgcolor': colors['background'],\
                'font': {
                    'color': colors['text']
                },
                'title': 'Dash Data Visualization'\
                }
        }
    )   
])

if __name__ == '__main__':
    app.scripts.config.serve_locally=True
    app.run_server(host='0.0.0.0',debug=True)





