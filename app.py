import dash
import dash_core_components as dcc
import dash_html_components as html
import numpy as np
import pandas as pd
import plotly.graph_objs as go
import subprocess
import textwrap
import flask
import os
import datetime as dt
from flask_caching import Cache

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
timeout = 60

#change to local address
df = pd.read_csv('https://gitlab.com/tonychang/sentimentApp/raw/master/data/predictions_no_dup_v1_sml.csv' , sep='|')
w = 70

def rand_jit(arr):
    stdev = .01*(max(arr)-min(arr))
    return(arr + np.random.randn(len(arr)) * stdev)

c = df.pred_sentiment.value_counts()

STARTING_ID = 'DOI-2017-0002-0002'
TEXT_DESCRIPTION = df.loc[df['id'] == STARTING_ID]['text'].iloc[0]

server = flask.Flask(__name__)
server.secret_key = os.environ.get('secret_key', 'secret')

app = dash.Dash(__name__, server=server, external_stylesheets=external_stylesheets)
cache = Cache(app.server, config={
    'CACHE_TYPE': 'filesystem',
    'CACHE_DIR': 'cache-directory'})
app.config.suppress_callback_exceptions = True

def create_trace(df=df):
    return(
        go.Scatter3d(
            x = rand_jit(df['0_prob']*100),
            y = rand_jit(df['1_prob']*100),
            z = rand_jit(df['2_prob']*100),
            text = df['id'],
            mode='markers',
            opacity=0.7,
            marker={
                'sizemode' : 'diameter',
                'size': 2,
                'color': df['pred_sentiment'],
                'colorscale': 'Viridis',
                'opacity' : 0.9,
            }
        ))

def create_scatterfigure(trace=create_trace()):
    return{
    'data': [trace],
    'layout': go.Layout(
        scene = dict( 
            xaxis={'title': 'Neutral (%)'},
            yaxis={'title': 'Positive (%)'},
            zaxis={'title': 'Negative (%)'}
            ),
        margin =dict(
            l=0,
            r=0,
            b=0,
            t=0),
        hovermode ='closest')}

trace = create_trace(df)
FIGURE = create_scatterfigure(trace)

app.layout = html.Div([
    # Row 1: Header and Intro text
                html.Div([
                    html.Div([
                        html.Img(src="https://conbio.org/images/smith/HeaderLogo_noline1.jpg",
                                style={
                                    'height': '100px',
                                    'float': 'left',
                                    'position': 'relative',
                                    'bottom': '10px',
                                    'left': '10px'
                                },
                                ),
                        ],className='row'),
                    html.Div([
                        html.H3('National Monuments',
                                style={
                                    'position': 'relative',
                                    'top': '0px',
                                    'left': '10px',
                                    'font-family': 'Dosis',
                                    'display': 'inline',
                                    'font-size': '6.0rem',
                                    'color': '#4D637F'
                                })],className='row'),
                    html.Div([
                    html.H3('Sentiment Analysis',
                            style={
                                'position': 'relative',
                                'top': '0px',
                                'left': '10px',
                                'font-family': 'Dosis',
                                'display': 'inline',
                                'font-size': '6.0rem',
                                'color': '#4D637F'
                            })],className='row')
                ],className='row twelve columns', style={'position': 'relative', 'right': '15px'}),
    # row 2 figures
    #hover panel
    html.Div([
        html.Div([ 
            html.H4('Point Summary',style={'position':'relative','left':'30%'}),
            html.Div([
                dcc.Graph(id='piedata'),
                        ])
        ],className='four columns'),
        #plot panel
        html.Div([
            html.H4('Testset 3D Scatter',style={'position':'relative','left':'30%'}),
            dcc.Graph(
                id='scattergraph',
                figure=FIGURE)
        ], className='eight columns')
    ],className='row'),

    html.Div([
        html.B('Comment:    '),
        html.A(STARTING_ID,
            id='idname',
            href="https://www.regulations.gov/document?D=DOI-2017-0002-0002",
            target='_blank'),
        html.Br(),
        html.B('Predicted Sentiment:    '),
        html.B('None',id='sentext'),
        html.Br(),
        html.B('Duplicate Count:    '),
        html.B('1',id='duptext'),
        html.P(TEXT_DESCRIPTION,
            id='text',
            style=dict(maxHeight='800px', fontSize='13px',backgroundColor='#D3D3D3')),
        ], className='row ten columns')
])    
#######FUNCTIONS################# 

def create_piedata(dff):
    return(dict(
        data = [go.Pie(
                values=np.round(np.array(dff),decimals=2),
                textinfo='value',
                labels=['Pos %', 'Neg %', 'Neu %'],
                hole=0.4,
                marker=dict(colors=['#20908C','#FDE724','#440154'],
                        line=dict(color='#000000',width=1))
                )],
        layout = go.Layout(
            margin=dict(l=5, b=20, t=20, r=0),
            legend=dict(orientation='h', xanchor='right',yanchor='top',y=1)
        )
        )
    )

def dfRowFromHover(hoverData):
    ''' Returns row for hover point as a Pandas Series '''
    if hoverData is not None:
        if 'points' in hoverData:
            firstPoint = hoverData['points'][0]
            if 'pointNumber' in firstPoint:
                point_number = firstPoint['pointNumber']
                id_name = str(FIGURE['data'][0]['text'][point_number]).strip()
                return df.loc[df['id'] == id_name]
    return pd.Series()

@app.callback(
    dash.dependencies.Output('piedata', 'figure'),
    [dash.dependencies.Input('scattergraph', 'hoverData')])
@cache.memoize(timeout=timeout)
def update_pie(hoverData):
    neg_v = hoverData['points'][0]['x']
    pos_v = hoverData['points'][0]['y']
    neu_v = hoverData['points'][0]['z']
    dff = [pos_v, neu_v, neg_v]
    return create_piedata(dff)


@app.callback(
    dash.dependencies.Output('idname', 'children'),
    [dash.dependencies.Input('scattergraph', 'hoverData')])
@cache.memoize(timeout=timeout)
def return_id_name(hoverData):
    if hoverData is not None:
        if 'points' in hoverData:
            firstPoint = hoverData['points'][0]
            if 'pointNumber' in firstPoint:
                point_number = firstPoint['pointNumber']
                id_name = str(FIGURE['data'][0]['text'][point_number]).strip()
                return id_name

@app.callback(
    dash.dependencies.Output('idname', 'href'),
    [dash.dependencies.Input('scattergraph', 'hoverData')])
@cache.memoize(timeout=timeout)
def return_href(hoverData):
    row = dfRowFromHover(hoverData)
    if row.empty:
        return
    datasheet_link = row['url'].iloc[0]
    return datasheet_link

@app.callback(
    dash.dependencies.Output('text', 'children'),
    [dash.dependencies.Input('scattergraph', 'hoverData')])
@cache.memoize(timeout=timeout)
def display_pointtext(hoverData):
    row = dfRowFromHover(hoverData)
    if row.empty:
        return
    description = row['text'].iloc[0]
    return description

@app.callback(
    dash.dependencies.Output('duptext', 'children'),
    [dash.dependencies.Input('scattergraph', 'hoverData')])
@cache.memoize(timeout=timeout)
def display_point_duplicate_count(hoverData):
    row = dfRowFromHover(hoverData)
    if row.empty:
        return
    dupcount = row['dup_count'].iloc[0]
    return dupcount

@app.callback(
    dash.dependencies.Output('sentext', 'children'),
    [dash.dependencies.Input('scattergraph', 'hoverData')])
@cache.memoize(timeout=timeout)
def display_point_sentiment(hoverData):
    row = dfRowFromHover(hoverData)
    if row.empty:
        return
    sent_i = row['pred_sentiment'].iloc[0]
    slabel = ['Neutral','Positive','Negative']
    out = str(slabel[sent_i])
    return out

if __name__ == '__main__':
    app.run_server(debug=True, threaded=True)
    #app.run_server(debug=True, threaded=True, host='0.0.0.0', port=8050)
